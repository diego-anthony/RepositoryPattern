﻿namespace Repository
{
    public interface IUnitOfWork: IRepository
    {
        int Save();
    }
}
