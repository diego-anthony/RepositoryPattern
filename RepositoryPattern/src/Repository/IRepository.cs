﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Repository
{
    public interface IRepository : IDisposable
    {
        T Create<T>(T newEntity) where T : class;
        bool Update<T>(T modiefEntity) where T : class;
        bool Delete<T>(T deletedEntity) where T : class;
        T FindEntity<T>(Expression<Func<T, bool>> expression) where T : class;
        IEnumerable<T> FindEntitySet<T>(Expression<Func<T, bool>> expression) where T : class;
    }
}
