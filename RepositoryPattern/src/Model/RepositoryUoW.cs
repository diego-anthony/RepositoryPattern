﻿using Repository;
using System;

namespace Model
{
    public class RepositoryUoW: EFRepository.RepositoryUoW,IUnitOfWork, IDisposable
    {
        public RepositoryUoW(
           bool autoDetectChangesEnabled = false,
           bool proxyCreationEnabled = false) : base(
               new DeleteMeContext(),
               autoDetectChangesEnabled,
               proxyCreationEnabled)
        {
        }
    }
}
