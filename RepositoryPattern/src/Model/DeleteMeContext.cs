namespace Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DeleteMeContext : DbContext
    {
        public DeleteMeContext()
            : base("name=DeleteMeContext")
        {
        }

        public virtual DbSet<country> country { get; set; }
        public virtual DbSet<day_week> day_week { get; set; }
        public virtual DbSet<departament> departament { get; set; }
        public virtual DbSet<district> district { get; set; }
        public virtual DbSet<horario> horario { get; set; }
        public virtual DbSet<image_location> image_location { get; set; }
        public virtual DbSet<location> location { get; set; }
        public virtual DbSet<location_statistic> location_statistic { get; set; }
        public virtual DbSet<permission> permission { get; set; }
        public virtual DbSet<role> role { get; set; }
        public virtual DbSet<state> state { get; set; }
        public virtual DbSet<statistic> statistic { get; set; }
        public virtual DbSet<type_location> type_location { get; set; }
        public virtual DbSet<user_info> user_info { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<country>()
                .Property(e => e.txt_code)
                .IsUnicode(false);

            modelBuilder.Entity<country>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<day_week>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<day_week>()
                .HasMany(e => e.horario)
                .WithMany(e => e.day_week)
                .Map(m => m.ToTable("day_horario").MapLeftKey("id_day_week").MapRightKey("id_horario"));

            modelBuilder.Entity<departament>()
                .Property(e => e.txt_code)
                .IsUnicode(false);

            modelBuilder.Entity<departament>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<departament>()
                .HasMany(e => e.district)
                .WithRequired(e => e.departament)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<district>()
                .Property(e => e.txt_code)
                .IsUnicode(false);

            modelBuilder.Entity<district>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<district>()
                .HasMany(e => e.location)
                .WithRequired(e => e.district)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<image_location>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<image_location>()
                .Property(e => e.txt_description)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.num_ruc)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_abbrev)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_description)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.latitude)
                .HasPrecision(8, 6);

            modelBuilder.Entity<location>()
                .Property(e => e.longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_email1)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_email2)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.phone1)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.phone2)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_direction1)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_direction2)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_web_site1)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.txt_web_site2)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .HasMany(e => e.horario)
                .WithRequired(e => e.location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<location>()
                .HasMany(e => e.image_location)
                .WithRequired(e => e.location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<location>()
                .HasMany(e => e.location_statistic)
                .WithRequired(e => e.location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<location_statistic>()
                .Property(e => e.txt_description)
                .IsUnicode(false);

            modelBuilder.Entity<permission>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<permission>()
                .Property(e => e.txt_description)
                .IsUnicode(false);

            modelBuilder.Entity<permission>()
                .HasMany(e => e.role)
                .WithMany(e => e.permission)
                .Map(m => m.ToTable("permission_role").MapLeftKey("id_permission").MapRightKey("id_role"));

            modelBuilder.Entity<role>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<role>()
                .Property(e => e.txt_description)
                .IsUnicode(false);

            modelBuilder.Entity<role>()
                .HasMany(e => e.user_info)
                .WithMany(e => e.role)
                .Map(m => m.ToTable("role_user").MapLeftKey("id_role").MapRightKey("id_user_info"));

            modelBuilder.Entity<state>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<state>()
                .Property(e => e.txt_description)
                .IsUnicode(false);

            modelBuilder.Entity<state>()
                .HasMany(e => e.location)
                .WithRequired(e => e.state)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<statistic>()
                .Property(e => e.txt_abbrev)
                .IsUnicode(false);

            modelBuilder.Entity<statistic>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<statistic>()
                .Property(e => e.txt_description)
                .IsUnicode(false);

            modelBuilder.Entity<statistic>()
                .HasMany(e => e.location_statistic)
                .WithRequired(e => e.statistic)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<type_location>()
                .Property(e => e.txt_abbrev)
                .IsUnicode(false);

            modelBuilder.Entity<type_location>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<type_location>()
                .Property(e => e.txt_description)
                .IsUnicode(false);

            modelBuilder.Entity<type_location>()
                .HasMany(e => e.location)
                .WithRequired(e => e.type_location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<user_info>()
                .Property(e => e.txt_name)
                .IsUnicode(false);

            modelBuilder.Entity<user_info>()
                .Property(e => e.txt_ap_paterno)
                .IsUnicode(false);

            modelBuilder.Entity<user_info>()
                .Property(e => e.txt_ap_materno)
                .IsUnicode(false);

            modelBuilder.Entity<user_info>()
                .Property(e => e.txt_user)
                .IsUnicode(false);

            modelBuilder.Entity<user_info>()
                .Property(e => e.txt_email)
                .IsUnicode(false);

            modelBuilder.Entity<user_info>()
                .Property(e => e.password_hash)
                .IsUnicode(false);

            modelBuilder.Entity<user_info>()
                .Property(e => e.phone)
                .IsUnicode(false);
        }
    }
}
