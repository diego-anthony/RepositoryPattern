namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class type_location
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public type_location()
        {
            location = new HashSet<location>();
        }

        [Key]
        public int id_type_location { get; set; }

        [StringLength(10)]
        public string txt_abbrev { get; set; }

        [Required]
        [StringLength(150)]
        public string txt_name { get; set; }

        [StringLength(300)]
        public string txt_description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<location> location { get; set; }
    }
}
