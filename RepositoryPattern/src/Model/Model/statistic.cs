namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("statistic")]
    public partial class statistic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public statistic()
        {
            location_statistic = new HashSet<location_statistic>();
        }

        [Key]
        public int id_statistic { get; set; }

        [StringLength(10)]
        public string txt_abbrev { get; set; }

        [Required]
        [StringLength(150)]
        public string txt_name { get; set; }

        [StringLength(300)]
        public string txt_description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<location_statistic> location_statistic { get; set; }
    }
}
