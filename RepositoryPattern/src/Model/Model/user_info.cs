namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class user_info
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public user_info()
        {
            role = new HashSet<role>();
        }

        [Key]
        public int id_user_info { get; set; }

        [StringLength(150)]
        public string txt_name { get; set; }

        [StringLength(150)]
        public string txt_ap_paterno { get; set; }

        [StringLength(150)]
        public string txt_ap_materno { get; set; }

        [StringLength(150)]
        public string txt_user { get; set; }

        [Required]
        [StringLength(320)]
        public string txt_email { get; set; }

        [Required]
        [StringLength(150)]
        public string password_hash { get; set; }

        [StringLength(15)]
        public string phone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<role> role { get; set; }
    }
}
