namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("district")]
    public partial class district
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public district()
        {
            location = new HashSet<location>();
        }

        [Key]
        public int id_district { get; set; }

        [StringLength(10)]
        public string txt_code { get; set; }

        [Required]
        [StringLength(150)]
        public string txt_name { get; set; }

        public int id_departament { get; set; }

        public virtual departament departament { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<location> location { get; set; }
    }
}
