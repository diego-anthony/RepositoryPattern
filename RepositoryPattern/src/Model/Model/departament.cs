namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("departament")]
    public partial class departament
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public departament()
        {
            district = new HashSet<district>();
        }

        [Key]
        public int id_departament { get; set; }

        [StringLength(10)]
        public string txt_code { get; set; }

        [Required]
        [StringLength(150)]
        public string txt_name { get; set; }

        public int? id_country { get; set; }

        public virtual country country { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<district> district { get; set; }
    }
}
