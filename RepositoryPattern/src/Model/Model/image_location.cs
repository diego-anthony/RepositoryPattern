namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class image_location
    {
        [Key]
        public int id_image_location { get; set; }

        public bool is_main { get; set; }

        [StringLength(150)]
        public string txt_name { get; set; }

        [Column(TypeName = "image")]
        public byte[] data_image { get; set; }

        [StringLength(300)]
        public string txt_description { get; set; }

        public int id_location { get; set; }

        public virtual location location { get; set; }
    }
}
