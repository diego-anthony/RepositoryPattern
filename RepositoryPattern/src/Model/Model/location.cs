namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("location")]
    public partial class location
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public location()
        {
            horario = new HashSet<horario>();
            image_location = new HashSet<image_location>();
            location_statistic = new HashSet<location_statistic>();
        }

        [Key]
        public int id_location { get; set; }

        [StringLength(11)]
        public string num_ruc { get; set; }

        [StringLength(10)]
        public string txt_abbrev { get; set; }

        [Required]
        [StringLength(150)]
        public string txt_name { get; set; }

        [StringLength(300)]
        public string txt_description { get; set; }

        public decimal? latitude { get; set; }

        public decimal? longitude { get; set; }

        [StringLength(320)]
        public string txt_email1 { get; set; }

        [StringLength(320)]
        public string txt_email2 { get; set; }

        [StringLength(15)]
        public string phone1 { get; set; }

        [StringLength(15)]
        public string phone2 { get; set; }

        [StringLength(200)]
        public string txt_direction1 { get; set; }

        [StringLength(200)]
        public string txt_direction2 { get; set; }

        [StringLength(200)]
        public string txt_web_site1 { get; set; }

        [StringLength(200)]
        public string txt_web_site2 { get; set; }

        public DateTime? expired_date { get; set; }

        public int id_type_location { get; set; }

        public int id_district { get; set; }

        public int id_state { get; set; }

        public virtual district district { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<horario> horario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<image_location> image_location { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<location_statistic> location_statistic { get; set; }

        public virtual state state { get; set; }

        public virtual type_location type_location { get; set; }
    }
}
