namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("horario")]
    public partial class horario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public horario()
        {
            day_week = new HashSet<day_week>();
        }

        [Key]
        public int id_horario { get; set; }

        public TimeSpan opening_time { get; set; }

        public TimeSpan closing_time { get; set; }

        public bool? day_complet { get; set; }

        public int id_location { get; set; }

        public virtual location location { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<day_week> day_week { get; set; }
    }
}
