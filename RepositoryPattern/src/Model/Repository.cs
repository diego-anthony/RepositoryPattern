﻿using Repository;
using System;

namespace Model
{
    public class Repository : EFRepository.Repository,IDisposable, IRepository
    {
        public Repository(
            bool autoDetectChangesEnabled = false,
            bool proxyCreationEnabled = false):base(
                new DeleteMeContext(),
                autoDetectChangesEnabled,
                proxyCreationEnabled)
        {
        }
    }
}
