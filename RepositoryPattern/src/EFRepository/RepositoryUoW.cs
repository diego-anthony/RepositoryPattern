﻿using Repository;
using System;
using System.Data.Entity;

namespace EFRepository
{
    public class RepositoryUoW : Repository, IRepository,IUnitOfWork,IDisposable
    {
        private DbContext _context;
        public RepositoryUoW(DbContext context,
            bool autoDetectedChangesEnabled = false,
            bool ProxyCreationEnabled = false) : base(
                context, autoDetectedChangesEnabled, ProxyCreationEnabled)
        {
            _context = context;
        }

        protected override int TrySaveChanges()
        {
            return 0;
        }

        public int Save()
        {
            int result = 0;
            try
            {
                result = _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw(ex);
            }
            return result;
        }
    }
}
