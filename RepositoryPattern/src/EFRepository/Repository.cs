﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Repository;

namespace EFRepository
{
    public class Repository : IRepository, IDisposable
    {
        private DbContext _context;
        public Repository(DbContext context,
            bool autoDetectedChangesEnabled = false,
            bool ProxyCreationEnabled = false)
        {
            _context = context;
            _context.Configuration.AutoDetectChangesEnabled = autoDetectedChangesEnabled;
            _context.Configuration.ProxyCreationEnabled = ProxyCreationEnabled;
        }
        public T Create<T>(T newEntity) where T : class
        {
            T result = null;
            try
            {
                result = _context.Set<T>().Add(newEntity);
                TrySaveChanges();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return result;
        }

        protected virtual int TrySaveChanges()
        {
            return _context.SaveChanges();
        }

        public bool Delete<T>(T deletedEntity) where T : class
        {
            bool result = false;
            try
            {
                _context.Set<T>().Attach(deletedEntity);
                _context.Set<T>().Remove(deletedEntity);
                result = TrySaveChanges() > 0;
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            return result;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        public T FindEntity<T>(Expression<Func<T, bool>> expression) where T : class
        {
            T result = null;
            try
            {
                result = _context.Set<T>().FirstOrDefault(expression);
            }
            catch (Exception ex)
            {
                throw(ex);
            }
            return result;
        }

        public IEnumerable<T> FindEntitySet<T>(Expression<Func<T, bool>> expression) where T : class
        {
            List<T> result = null;
            try
            {
                result = _context.Set<T>().Where(expression).ToList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return result;
        }

        public bool Update<T>(T modifiedEntity) where T : class
        {
            bool result = false;
            try
            {
                _context.Set<T>().Attach(modifiedEntity);
                _context.Entry(modifiedEntity).State = EntityState.Modified;
                result = TrySaveChanges() > 0;
            }
            catch (Exception ex)
            {

                throw(ex);
            }
            return result;
        }
    }
}
